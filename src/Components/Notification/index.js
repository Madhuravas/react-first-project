import "./index.css"

const Notification =()=>{
    return(
        <div className="notification-card">
            <h1 className="notification-text1">Support Ukraine 🇺🇦 </h1>
            <h1 className="notification-text2">Help Provide Humanitarian Aid to Ukraine</h1>
        </div>
    )
}

export default Notification