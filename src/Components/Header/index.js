import './index.css'



const Header = () =>{
    return(
       <div className="header-container">
           <h1 className="header-heading">React</h1>
           <p className="header-description">A JavaScript library for building user interfaces</p>
           <div>
               <button className="header-button">Get Started</button>
               <p className="tutorial-text">Take the Tutorial <i class="fa fa-angle-right right"></i></p>
               
           </div>
       </div>
    )
}


export default Header