import "./index.css"

import lang from "./logo-lang.svg"

const Navbar = (props) => {
    const { logo } = props
    return (
        <nav className="navbar-card">
            <div className="logo-title-card">
                <img className="logo" src={logo} alt="logo" />
                <h1 className="logo-title">React</h1>
            </div>
            <ul className="nav-items">
                <li>Docs</li>
                <li>Tutorial</li>
                <li>Blog</li>
                <li>Community</li>
            </ul>
            <div className="mobile-search">
                <i class="search-icon fas fa-search"></i>
                <img className="logo-lang" src={lang} alt="language" />
            </div>
            <div className="search-card">
                <i class="search-icon fas fa-search"></i>
                <input className="input-box" type="text" placeholder="Search" />
                <p className="version-text">v18.2.0</p>
            </div>
            <div className="lang-card">
                <img className="logo-lang" src={lang} alt="language" />
                <p className="lang-text">Language</p>
            </div>
            <p className="git-text">Github</p>
        </nav>
    )
}

export default Navbar