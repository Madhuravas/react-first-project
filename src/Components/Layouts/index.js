import "./index.css"


const Layouts = () => {
    return (
        <div className="layouts-container">
            <div>
                <h1 className="layout-heading">Declarative</h1>
                <p className="layout-description">React makes it painless to create interactive UIs. Design simple views for each state in your application, and React will efficiently update and render just the right components when your data changes.
                    <br /><br />  Declarative views make your code more predictable and easier to debug.</p>
            </div>
            <div>
                <h1 className="layout-heading">Component-Basec</h1>
                <p className="layout-description">Build encapsulated components that manage their own state, then compose them to make complex UIs.<br /> <br />
                    Since component logic is written in JavaScript instead of templates, you can easily pass rich data through your app and keep state out of the DOM.</p>
            </div>
            <div>
                <h1 className="layout-heading">Learn Once, Write Anywhere</h1>
                <p className="layout-description">We don’t make assumptions about the rest of your technology stack, so you can develop new features in React without rewriting existing code.<br/> <br/>
                    React can also render on the server using Node and power mobile apps using React Native.</p>
            </div>
        </div>
    )
}

export default Layouts