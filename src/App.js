import Notification from './Components/Notification';
import Navbar from './Components/navbar';
import Header from './Components/Header'
import Layouts from './Components/Layouts';

import logo from './logo.svg';



import './App.css';

function App() {
  return (
    <div>
      <Notification/>
      <Navbar logo={logo} />
      <Header/>
      <Layouts/>
    </div>
  );
}

export default App;
